import { Component, OnInit } from '@angular/core';
import { SampledataService } from './sampledata.service';
import { PagerService } from './pager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  sampleData: any;
    // pager object
  pager: any = {};
    // paged items
  pagedItems: any[];
  pageSize: number = 10;
  pageSizeItems: any = [10, 20, 30, 40, 50]

  constructor(private sampledataService: SampledataService, private pagerService:PagerService) { }

  setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.sampleData.length, page,10,10 );
    // get current page of items
    this.pagedItems = this.sampleData.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  changePageSize(value: number) {
    this.pageSize = +value;
    this.setPage(1);
  }

  ngOnInit(){
    this.sampledataService.GetSampleData()
    .subscribe(data => {
      this.sampleData = data;
      //log response to console.
      console.log(data);
      this.setPage(1);
    });
  }
  title = 'Angular Assessment';
}
