import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SampledataService {

  constructor(private httpClient: HttpClient) { }
  GetSampleData(): Observable<any[]> {
    return this.httpClient.get<any[]>('assets/sample_data.json');
  }

}
