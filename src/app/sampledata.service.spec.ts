import { TestBed } from '@angular/core/testing';

import { SampledataService } from './sampledata.service';

describe('SampledataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SampledataService = TestBed.get(SampledataService);
    expect(service).toBeTruthy();
  });
});
